import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import A from "../views/a.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "a",
    component: A
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
